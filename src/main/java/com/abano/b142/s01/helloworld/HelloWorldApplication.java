package com.abano.b142.s01.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HelloWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldApplication.class, args);
	}

    @GetMapping("hi/{user}")
    public String hi(@PathVariable("user") String user) {
        return String.format("Hi user, %s!", user);
    }

	@GetMapping("api/hello")
	public String hello() {
		return String.format("Hello");
	}

	@GetMapping("api/good-evening")
	public String goodEvening() {
		return String.format("good evening!");
	}

	//http://locahost:8080/api/hello-name?name=<name-input>
	@GetMapping("api/hello-name")
	public String helloName(@RequestParam(value="name", defaultValue = "World") String name) {
		return String.format("Hello, %s! Welcome.", name);
	}

	// http://localhost:8080/api/your-name/Xenia
	// http://localhost:8080/api/your-name/<name_input>
	@GetMapping("api/your-name/{name}")
	public String yourName(@PathVariable("name") String name) {
		return String.format("Hello, %S! How are you doing today?", name);
	}


}
